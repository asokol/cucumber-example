package com.hello;

import com.hello.repositories.HelloRepository;
import com.hello.service.HelloService;

public class Main {
    public static void main(String[] args) {
        HelloRepository helloRepository = new HelloRepository();
        HelloService helloService = new HelloService(helloRepository);

        System.out.println(helloRepository.getGreetingText());
    }
}
