package com.hello.repositories;

public class HelloRepository {

    private final String text;

    public HelloRepository() {
        this.text = "Hello World";
    }

    public String getGreetingText(){
        return text;
    }
}
