package com.hello.service;

import com.hello.repositories.HelloRepository;

public class HelloService {

    private final HelloRepository helloRepository;

    public HelloService(HelloRepository helloRepository) {
        this.helloRepository = helloRepository;
    }

    public String saySomething(){
        return helloRepository.getGreetingText();
    }
}
