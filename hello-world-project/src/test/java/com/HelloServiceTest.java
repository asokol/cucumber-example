package com;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.hello.repositories.HelloRepository;
import com.hello.service.HelloService;

public class HelloServiceTest {

    @Test
    public void helloServiceCanReturnValidValue(){

        HelloRepository helloRepo = mock(HelloRepository.class);
        when(helloRepo.getGreetingText()).thenReturn("Test!");

        HelloService helloService = new HelloService(helloRepo);
        Assertions.assertEquals(helloService.saySomething(), "Test!");
    }
}
